# Java Fraud Detector Demo

This is a sample solution to the coding challenge :rocket:

## Prerequisites
You will need the following to run this project:
1. A laptop or desktop machine with internet access
2. A relatively recent JDK
3. Project built in IntelliJ but code could be edited with and editor.

## Setting Up
* Open the project folder using IntelliJ IDE

## How to run
* To read transactions from file and detect frauds use the cmd line option 'process'. Sample cmd look like this:
 - java Main process /tmp/test_input_long.txt 2018-06-18T07:58:26 8000.00
 
This command will read transaction from the file test_input_long.txt and detect with threadshold 8000.00 on day 2018-06-18

* Optionally, to generate random transactions and write into files use the cmd line option 'generate'. Sample cmd look like this:
 - java Main generate /tmp/test_input_long.txt 60000 999.99
 
 