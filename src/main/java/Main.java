import com.fraud.pay.detector.AfterFraudDetector;
import com.fraud.pay.detector.FraudDetector;
import com.fraud.pay.emulator.TransactionGenerator;
import com.fraud.pay.money.Price;
import com.fraud.pay.time.DateFormatter;
import com.fraud.pay.time.FraudPayDateFormatter;
import com.fraud.pay.time.ServerTime;
import com.fraud.pay.transaction.Transaction;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This file is added only for illustration purposes. Not meant to be used for production.
 */
public class Main {

    private static final long MILLISECONDS_IN_A_SECOND = 1000L;
    private static final long MILLISECONDS_IN_A_MINUTE = 60L * MILLISECONDS_IN_A_SECOND;
    private static final long MILLISECONDS_IN_AN_HOUR = 60L * MILLISECONDS_IN_A_MINUTE;
    private static final long MILLISECONDS_IN_A_DAY = 24L * MILLISECONDS_IN_AN_HOUR;
    private static final long MILLISECONDS_IN_A_WEEK = 7L * MILLISECONDS_IN_A_DAY;
    private static final int POOL_SIZE = 12; // @TODO: read from config?
    private static final int REQUIRED_LENGTH = 4;
    private static final String PROCESS = "process";
    private static final String GENERATE = "generate";

    /**
     * Test run with input file and other needed parameters. Illustration only ...
     *
     * @param args Command line parameters.
     */
    public static void main(String[] args) throws IOException, ParseException {
        requireArgsNum(args);

        switch (args[0]) {
            case PROCESS: {
                FraudDetector fraudDetector = new AfterFraudDetector();
                fraudDetector.detect(readTransactions(args[1]), readDate(args[2]), Price.parse(args[3]))
                             .forEach(System.out::println);
                break;
            }
            case GENERATE: {
                generateRandomTests(args[1], Integer.parseInt(args[2]), new BigDecimal(args[3]));
                break;
            }
            default:
                System.out.println("Error, usage: java ClassName read inputfile");
                System.exit(1);
        }
    }

    private static void requireArgsNum(String[] args) {
        if (args.length < Main.REQUIRED_LENGTH) {
            System.out.println("Error, not enough arguments");
            System.exit(1);
        }
    }

    private static ArrayList<Transaction> readTransactions(String filePath) throws IOException {
        FraudPayDateFormatter fraudPayDateFormatter = new FraudPayDateFormatter();
        ArrayList<Transaction> transactions = new ArrayList<>();
        Files.lines(new File(filePath).toPath())
             .map(s -> s.split(","))
             .filter(ss -> ss.length == 3)
             .map(ss -> {
                 String hash = ss[0].trim();
                 ServerTime serverTime = null;
                 try {
                     serverTime = ServerTime.create(fraudPayDateFormatter.parse(ss[1].trim()));
                 } catch (ParseException e) {
                     e.printStackTrace();
                 }
                 Price price = Price.parse(ss[2].trim());
                 return new Transaction(hash, serverTime, price);
             })
             .forEach(transactions::add);
        return transactions;
    }

    private static Date readDate(String dateString) throws ParseException {
        DateFormatter dateFormatter = new FraudPayDateFormatter();
        return dateFormatter.parse(dateString);
    }

    private static void generateRandomTests(String filePath, int numTransactions, BigDecimal priceRange)
            throws IOException {
        FileWriter fileWriter = new FileWriter(new File(filePath));

        // Some parameters are hardcoded only for the coding test ...
        TransactionGenerator transactionGenerator =
                new TransactionGenerator(POOL_SIZE, priceRange, MILLISECONDS_IN_A_WEEK);
        for (int i = 0; i < numTransactions; i++) {
            Transaction transaction = transactionGenerator.randTransaction();
            fileWriter.write(transaction.getHash() +
                    ", " + transaction.getTimeStamp().format() +
                    ", " + transaction.getPrice().format() + "\n");
        }
        fileWriter.close();
    }
}
