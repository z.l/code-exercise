package com.fraud.pay.money;

import java.math.BigDecimal;
import java.util.Currency;
import java.util.Locale;

public final class Price {
    private final Currency currency;
    private final BigDecimal amount;

    private Price(Currency currency, BigDecimal amount) {
        this.currency = currency;
        this.amount = amount;
    }

    public static Price parse(String source) {
        Currency instance = Currency.getInstance(Locale.US);
        return new Price(instance, new BigDecimal(source));
    }

    public static Price add(Price price1, Price price2) {
        if (!price1.currency.equals(price2.currency)) {
            throw new InternalError("Currency data not match");
        }

        return new Price(price1.currency, price1.amount.add(price2.amount));
    }

    public String format() {
        return amount.toPlainString();
    }

    public int compareTo(Price val) {
        return amount.compareTo(val.amount);
    }
}
