package com.fraud.pay.detector;

import com.fraud.pay.money.Price;
import com.fraud.pay.transaction.Transaction;

import java.util.Date;
import java.util.List;

public interface FraudDetector {
    List<String> detect(List<Transaction> transactions, Date date, Price priceThreshold);
}
