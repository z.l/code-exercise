package com.fraud.pay.detector;

import com.fraud.pay.money.Price;
import com.fraud.pay.time.ServerTime;
import com.fraud.pay.transaction.Transaction;

import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class AfterFraudDetector implements FraudDetector {

    @Override
    public List<String> detect(List<Transaction> transactions, Date date, Price priceThreshold) {
        if (transactions == null || date == null || priceThreshold == null) { return Collections.emptyList(); }

        ServerTime serverTime = ServerTime.create(date);
        Map<String, Price> hashPriceMap = transactions.stream()
                .filter(transaction -> transaction.getTimeStamp().onSameDay(serverTime))
                .collect(Collectors.toMap(Transaction::getHash, Transaction::getPrice, Price::add));

        return hashPriceMap.entrySet().stream()
                .filter(it -> it.getValue().compareTo(priceThreshold) > 0)
                .map(Map.Entry::getKey)
                .collect(Collectors.toList());
    }
}
