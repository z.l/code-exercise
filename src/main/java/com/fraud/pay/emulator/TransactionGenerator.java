package com.fraud.pay.emulator;

import com.fraud.pay.money.Price;
import com.fraud.pay.time.ServerTime;
import com.fraud.pay.transaction.Transaction;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class TransactionGenerator {
    private final BigDecimal maxBigDecimal;
    private final long milliSecondRange;
    private final List<String> hashPool;

    private Random rand = new Random();

    public TransactionGenerator(int poolSize, BigDecimal maxBigDecimal, long milliSecondRange) {
        this.maxBigDecimal = maxBigDecimal;
        this.milliSecondRange = milliSecondRange;
        Stream<String> stream = Stream.generate(() -> UUID.randomUUID().toString()).limit(poolSize);
        hashPool  = stream.collect(Collectors.toCollection(ArrayList::new));
    }

    public Transaction randTransaction() {
        String hash = hashFromPool();
        Date date = new Date();
        date.setTime(System.currentTimeMillis() - randomLong());
        ServerTime serverTime = ServerTime.create(date);
        Price price = Price.parse(generateBigDecimal().toPlainString());
        return new Transaction(hash, serverTime, price);
    }

    private String hashFromPool() {
        return hashPool.get(rand.nextInt(hashPool.size()));
    }

    private long randomLong() {
        return ThreadLocalRandom.current().nextLong(milliSecondRange);
    }

    private BigDecimal generateBigDecimal() {
        BigDecimal randFromDouble = new BigDecimal(Math.random());
        BigDecimal actualRandomDec = randFromDouble.multiply(maxBigDecimal);
        return actualRandomDec.setScale(2, RoundingMode.HALF_UP);
    }
}
