package com.fraud.pay.time;

import java.text.ParseException;
import java.util.Date;

public interface DateFormatter {
    Date parse(String source) throws ParseException;

    String format(Date date);
}
