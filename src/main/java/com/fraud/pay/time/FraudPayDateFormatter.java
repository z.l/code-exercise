package com.fraud.pay.time;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class FraudPayDateFormatter implements DateFormatter {
    private static final String FRAUD_PAY_SIMPLE_DATE_FORMAT = "yyyy-MM-dd_HH:mm:ss";
    private static final String FORMAT_REPLACE = "_";
    private static final String PARSE_REPLACE = "T";

    private final SimpleDateFormat simpleDateFormat;

    public FraudPayDateFormatter() {
        simpleDateFormat = new SimpleDateFormat(FRAUD_PAY_SIMPLE_DATE_FORMAT);
    }

    @Override
    public Date parse(String source) throws ParseException {
        return simpleDateFormat.parse(source.replaceFirst(PARSE_REPLACE, FORMAT_REPLACE));
    }

    @Override
    public String format(Date date) {
        return simpleDateFormat.format(date).replaceFirst(FORMAT_REPLACE, PARSE_REPLACE);
    }
}
