package com.fraud.pay.time;

import org.jetbrains.annotations.NotNull;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.Objects;

public class ServerTime {
    @NotNull
    private final DateFormatter dateformatter;
    @NotNull
    private Date date;

    private ServerTime(@NotNull DateFormatter dateformatter, @NotNull Date date) {
        Objects.requireNonNull(dateformatter);
        Objects.requireNonNull(date);
        this.dateformatter = dateformatter;
        this.date = date;
    }

    public static ServerTime create(@NotNull Date date) {
        Objects.requireNonNull(date);
        return new ServerTime(new FraudPayDateFormatter(), date);
    }

    public final String format() {
        return dateformatter.format(date);
    }

    /**
     * Because of anomalies such as Daylight Saving Time (DST), days vary in length, not always 24 hours long.
     */
    public final boolean onSameDay(ServerTime time2) {
        return this.getYear() == time2.getYear() &&
                this.getMonth() == time2.getMonth() &&
                this.getDay() == time2.getDay();
    }

    public long getMilliSeconds() {
        return date.getTime();
    }

    private int getYear() {
        return getLocalDate(date).getYear();
    }

    private int getMonth() {
        return getLocalDate(date).getMonthValue();
    }

    private int getDay() {
        return getLocalDate(date).getDayOfMonth();
    }

    private LocalDate getLocalDate(Date date) {
        return date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
    }
}
