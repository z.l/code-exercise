package com.fraud.pay.transaction;

import com.fraud.pay.money.Price;
import com.fraud.pay.time.ServerTime;

public class Transaction {
    private String hash;
    private ServerTime timeStamp;
    private Price price;

    public Transaction(String hash, ServerTime timeStamp, Price price) {
        this.hash = hash;
        this.timeStamp = timeStamp;
        this.price = price;
    }

    public String getHash() {
        return hash;
    }

    public ServerTime getTimeStamp() {
        return timeStamp;
    }

    public Price getPrice() {
        return price;
    }
}
