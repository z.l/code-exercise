package com.fraud.pay.time;

import org.junit.Test;

import java.text.ParseException;
import java.util.Date;

import static org.junit.Assert.*;

public class ServerTimeTest {

    private static final String TEST_DAY_14_04_29_TIME_13_15_54 = "2014-04-29T13:15:54";
    private static final String TEST_DAY_14_04_29_TIME_03_15_54 = "2014-04-29T03:15:54";
    private static final String TEST_DAY_18_04_29_TIME_13_15_54 = "2018-04-29T13:15:54";

    private FraudPayDateFormatter fraudPayDateFormatter = new FraudPayDateFormatter();

    @Test
    public void create() throws ParseException {
        ServerTime serverTime = ServerTime.create(new Date());
        assertNotNull(serverTime);
    }

    @Test
    public void format() throws ParseException {
        Date date = fraudPayDateFormatter.parse(TEST_DAY_14_04_29_TIME_13_15_54);
        ServerTime serverTime = ServerTime.create(date);
        assertEquals(serverTime.format(), TEST_DAY_14_04_29_TIME_13_15_54);
    }

    @Test
    public void onSameDay_whenOnTheSameDay() throws ParseException {
        ServerTime serverTime1 = ServerTime.create(fraudPayDateFormatter.parse(TEST_DAY_14_04_29_TIME_13_15_54));
        ServerTime serverTime2 = ServerTime.create(fraudPayDateFormatter.parse(TEST_DAY_14_04_29_TIME_03_15_54));
        assertTrue(serverTime1.onSameDay(serverTime2));
    }

    @Test
    public void onSameDay_whenOnDifferentYears() throws ParseException {
        ServerTime serverTime1 = ServerTime.create(fraudPayDateFormatter.parse(TEST_DAY_14_04_29_TIME_13_15_54));
        ServerTime serverTime2 = ServerTime.create(fraudPayDateFormatter.parse(TEST_DAY_18_04_29_TIME_13_15_54));
        assertFalse(serverTime1.onSameDay(serverTime2));
    }

    @Test
    public void getMilliSeconds() {
        ServerTime serverTime = ServerTime.create(new Date());
        assertTrue(serverTime.getMilliSeconds() > 0L);
    }
}
