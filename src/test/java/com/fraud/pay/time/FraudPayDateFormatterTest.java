package com.fraud.pay.time;

import org.junit.Test;

import java.text.ParseException;
import java.util.Date;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class FraudPayDateFormatterTest {

    private FraudPayDateFormatter fraudPayDateFormatter = new FraudPayDateFormatter();

    @Test
    public void parse_whenSourceIsTformat() throws ParseException {
        Date date = fraudPayDateFormatter.parse("2014-04-29T13:15:54");
        assertNotNull(date);
    }

    @Test
    public void parse_whenSourceIsLowformat() throws ParseException {
        Date date = fraudPayDateFormatter.parse("2014-04-29_13:15:54");
        assertNotNull(date);
    }

    @Test
    public void format() throws ParseException {
        Date date = fraudPayDateFormatter.parse("2014-04-29T13:15:54");
        assertEquals(fraudPayDateFormatter.format(date), "2014-04-29T13:15:54");
    }
}
