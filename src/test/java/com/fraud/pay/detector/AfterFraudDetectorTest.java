package com.fraud.pay.detector;

import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertTrue;

public class AfterFraudDetectorTest {

    AfterFraudDetector afterFraudDetector = new AfterFraudDetector();

    @Test
    public void detect_whenNull() {
        List<String> fraudHashs = afterFraudDetector.detect(null, null, null);
        assertTrue(fraudHashs.isEmpty());
    }
}
