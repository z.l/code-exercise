package com.fraud.pay.money;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.junit.Assert.assertThat;

public class PriceTest {

    private Price price;

    @Test
    public void parse() {
        price = Price.parse("1.23");
        assert (price.format().equals("1.23"));
        assertThat(price, instanceOf(Price.class));
    }

    @Test
    public void add() {
        price = Price.add(Price.parse("1.23"), Price.parse("4.56")) ;
        assert (price.format().equals("5.79"));
    }
}
