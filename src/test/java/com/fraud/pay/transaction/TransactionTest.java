package com.fraud.pay.transaction;

import com.fraud.pay.money.Price;
import com.fraud.pay.time.ServerTime;
import org.junit.Test;

import java.util.Date;

import static org.junit.Assert.assertNotNull;

public class TransactionTest {

    Transaction transaction = new Transaction("some_test_hash_str", ServerTime.create(new Date()), Price.parse("1.23"));

    @Test
    public void getHash() {
        assertNotNull(transaction.getHash());
    }

    @Test
    public void getTimeStamp() {
        assertNotNull(transaction.getTimeStamp());
    }

    @Test
    public void getPrice() {
        assertNotNull(transaction.getPrice());
    }
}
